// $Id: editor_plugin.js,v 1.3.4.5 2008/12/27 12:42:53 sun Exp $

/*
	TO FIX'S
	
	v	when selected a list box item when the cursor is not set within the
		text area, the entire text area is removed

	v	when selected a new list box item when an existing one is select a new
		item is inserted within the old one
		
	v	now the problem is that the item is removed completely
		
	v	the book parent node should not be available as a chapter link
	
	-	because its an anchor (? or something else) when you drag and drop it
		it selects an internal area

*/

(function() {
	// Load plugin specific language pack.
	tinymce.PluginManager.requireLangPack('chapter_link');

	tinymce.create('tinymce.plugins.BookChapterLinkPlugin', {
		/**
		 * Initialize the plugin, executed after the plugin has been created.
		 *
		 * This call is done before the editor instance has finished it's
		 * initialization so use the onInit event of the editor instance to
		 * intercept that event.
		 *
		 * @param ed
		 *	 The tinymce.Editor instance the plugin is initialized in.
		 * @param url
		 *	 The absolute URL of the plugin location.
		 */
		init : function(ed, url) {
/*
			ed.onContextMenu.add(function(ed, e) {

			});

*/
			// Load CSS for editor contents on startup.
			ed.onInit.add(function() {
					ed.dom.loadCSS(url + "/css/chapter_link.css");
			});

			// Replace images with inline tags in editor contents upon data.save.
			ed.onBeforeGetContent.add(function(ed, data) {
				if (!data.save) {
					return;
				}
				jQuery.each(ed.dom.select('a', data.content), function(node) {
					if (this.name != 'mceItemChapterLink') {
						return;
					}
//					var inlineTag = '[chapter_link|' + decodeURIComponent(this.alt) + '|align=' + this.align + '|width=' + this.width + '|height=' + this.height + ']';
					var inlineTag = '[' + this.id.split('-').join('|') + ']';
					ed.dom.setOuterHTML(this, inlineTag);
				});
			});


			// Replace inline tags in data.content with chapter links.
			ed.onBeforeSetContent.add(function(ed, data) {
//				data.content = data.content.replace(/\[chapter_link\|([^\[\]]+)\]/g, function(orig, match) {
				data.content = data.content.replace(/\[node\|([^\[\]]+)\]/g, function(orig, match) {
					var node = {};
					
		//			node.href = 'node/' + match;

//	node.class breaks safari - capitalising is a work around
					node.CLASS = 'chapter-link';

//					node.setAttribute('class', 'chapter-link');
					node.id = 'node-' + match;
					node.name = 'mceItemChapterLink';
					var list_items = tinymce.util.JSON.parse(Drupal.settings.book_chapter_link.chapterlink);								

					text = 'node|' + match;
					for (var item in list_items) {
						if ( match == list_items[item].nid ) {
							text = list_items[item].chapter;
							break;
						}
					}
					
					return ed.dom.createHTML('a', node, text);
				});
				return;
			});

//	Add a node change handler - selects the existing menu item from the list box
			ed.onNodeChange.add(function(ed, cm, node) {
				if ( ed.dom.getAttrib(node, 'name') ) {
					if ( ed.dom.getAttrib(node, 'name').indexOf('mceItemChapterLink') != -1 ) {
//	v is the value of the list item to select
						v = node.id.split('-').join('|');
						cm.setActive('edit-body_chapter_list').select(v);
					}
				}
			});
		},

//	create the drop menu in the toolbar . populated from drupal.settings
		createControl: function(n, cm) {
			switch(n) {
				case 'chapter_link':
					var list_items = tinymce.util.JSON.parse(Drupal.settings.book_chapter_link.chapterlink);
					var list_box = cm.createListBox('chapter_list', {
						 title : 'Chapter link',
						 onselect : function(v) {
//	put tag after selected content ( if there is a selection )
//	if nothing is selected the entire content is removed.
//							tinyMCE.activeEditor.selection.setContent(tinyMCE.activeEditor.selection.getContent() + '[' + v + ']');
							var node = {};

//	work around for safari							
							node.CLASS = 'chapter-link mceItemAnchor';
							node.id = v.split('|').join('-');
							node.name = 'mceItemChapterLink';
							
//	look for the right chapter
							var list_items = tinymce.util.JSON.parse(Drupal.settings.book_chapter_link.chapterlink);								

							text = v;
							for (var item in list_items) {
								if ( v == 'node|' + list_items[item].nid ) {
									text = list_items[item].chapter;
									break;
								}
							}
							
//	are we updating an existing chapter link ? if so replace whats there
							if ( tinyMCE.activeEditor.selection.getNode().name == 'mceItemChapterLink' ) {
//	this is complex but nothing else seems to work
								old_node = tinyMCE.activeEditor.selection.getNode();
								bookmark = tinyMCE.activeEditor.selection.getBookmark(1);
								tinyMCE.activeEditor.dom.remove(old_node);
								tinyMCE.activeEditor.selection.moveToBookmark(bookmark);
								tinyMCE.activeEditor.selection.setContent(tinyMCE.activeEditor.dom.createHTML('a', node, text));
								return;
							}

//	otherwise please don't replace content
//	move the selection to the end of the range
//	if the area hasn't been touched yet this defaults to the start of the area
							tinyMCE.activeEditor.selection.collapse(false);
							tinyMCE.activeEditor.selection.setContent(tinyMCE.activeEditor.dom.createHTML('a', node, text));
						}
					});

//	add each value from the drupal settings to the listbox
					for (var item in list_items) {
						list_box.add(list_items[item].chapter + ' ' + list_items[item].title, 'node|' + list_items[item].nid);	
					}
								
//	Return the new listbox instance
					return list_box;
			}
			return null;
		},
		
		/**
		 * Return information about the plugin as a name/value array.
		 */
		getInfo : function() {
			return {
				longname : 'Book chapter link',
				author : 'Mr Snow',
				authorurl : 'http://www.houseoflaudanum.com',
				infourl : 'http://drupal.org/sandbox/queenvictoria/1850252',
				version : "1.1"
			};
		}
	});

	// Register plugin.
	tinymce.PluginManager.add('chapter_link', tinymce.plugins.BookChapterLinkPlugin);
//	tinymce.PluginManager.add('chapter_list', tinymce.plugins.BookChapterLinkPlugin);
})();
